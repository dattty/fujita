(function (w, d) {
    // namespace
    var _ns = (w.fjt = w.fjt || {});

    /**
     * スクロール禁止
     */
    var LockScroll = (_ns.LockScroll = _ns.LockScroll || {
        _$wrap: $(".wrap"),
        _scrollTop: 0,
        // スクロール禁止
        lock: function () {
            this._scrollTop = $(window).scrollTop();
            this._$wrap.addClass("is-lock");
            this._$wrap.css({
                top: -1 * this._scrollTop + "px",
            });
        },
        // スクロール禁止解除
        release: function () {
            this._$wrap.removeClass("is-lock");
            this._$wrap.css({
                top: 0,
            });
            w.scrollTo(0, this._scrollTop);
        },
    });

    /**
     * ヘッダー
     * @constructor
     */
    var Header = (_ns.Header =
        _ns.Header ||
        function () {
            this._init.apply(this, arguments);
        });
    Header.prototype._init = function () {
        this._$el = $(".js-header");
        this._switchNavPosition;
        this._$elTrigger = this._$el.find(".js-toggleMenu");
        // this._$elTarget  = this._$el.find('.global-nav__menu');
        this._$elPagetop = $(".js-pagetop");
        this._$elPagetopInner = $(".js-pagetop__inner");
        this._$elPagetopTrigger = $(".js-pagetop__trigger");
        this._$elBody = $("body");

        this._bindScroll();
        this._bindResize();
        this._bindClickTrigger();
    };
    Header.prototype._bindScroll = function () {
        this._setSwitchNavPosition();

        var startPos = 0;
        var winScrollTop = 0;
        var stateHide = "is-hide";
        $(window)
            .on(
                "scroll",
                function () {
                    // IEの場合は動作させない
                    if (!$("body").hasClass("is-ie")) {
                        // ヘッダー
                        winScrollTop = $(window).scrollTop();
                        if (winScrollTop < this._$el.outerHeight()) {
                            this._$el.removeClass(stateHide);
                        } else if (winScrollTop >= startPos) {
                            this._$el.addClass(stateHide);
                        } else {
                            this._$el.removeClass(stateHide);
                        }
                        startPos = winScrollTop;
                    }

                    // ページトップ
                    if (window.pageYOffset >= this._$elPagetop.offset().top - $(window).innerHeight() + this._$elPagetop.innerHeight() + (this._$elPagetopInner.css("top").split("px")[0] - 0)) {
                        this._$elPagetop.addClass("is-scroll-end");
                        this._$elPagetop.removeClass("is-show");
                    } else if (window.pageYOffset >= this._switchNavPosition) {
                        this._$elPagetop.addClass("is-show");
                        this._$elPagetop.removeClass("is-scroll-end");
                    } else {
                        this._$elPagetop.removeClass("is-scroll-end");
                        this._$elPagetop.removeClass("is-show");
                    }
                }.bind(this)
            )
            .trigger("scroll");
    };
    Header.prototype._bindResize = function () {
        $(window).on(
            "resize",
            function () {
                this._setSwitchNavPosition();

                var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                if (windowWidth > 960) {
                    this._$elBody.removeClass("is-menuShow");
                    _ns.LockScroll.release();
                }
            }.bind(this)
        );
    };
    Header.prototype._setSwitchNavPosition = function () {
        this._switchNavPosition = this._$el.innerHeight();
    };
    Header.prototype._bindClickTrigger = function () {
        $(this._$elTrigger).on(
            "click",
            function (e) {
                e.preventDefault();
                // 展開しているとき
                if (this._$elBody.hasClass("is-menuShow")) {
                    this._$elBody.removeClass("is-menuShow");
                    _ns.LockScroll.release();
                }
                // 閉じている時
                else {
                    this._$elBody.addClass("is-menuShow");
                    _ns.LockScroll.lock();
                }
            }.bind(this)
        );
    };

    var SideMenu = (_ns.SideMenu =
        _ns.SideMenu ||
        function (options) {
            this._init.apply(this, arguments);
        });
    SideMenu.prototype._init = function (options) {
        this._options = options || {};
        this._$elParent = $(".js-sideFix");
        if (this._$elParent.length === 0) {
            return;
        }
        this._$el = this._$elParent.length > 0 ? this._$elParent.find(".side__inner") : $(".side__inner");
        this._$elContent = $(".content__inner");
        this._$elMainContent = $(".main");
        this._changeTopTimer = null;

        $(window)
            .on(
                "resize load",
                function () {
                    this._adjustMainContent();
                }.bind(this)
            )
            .trigger("resize");
        setTimeout(
            function () {
                this._bindScrollWindow();
            }.bind(this),
            833
        );
    };
    SideMenu.prototype._adjustMainContent = function () {
        var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

        if (windowWidth > 960) {
            this._$elContent.css("min-height", this._$el.outerHeight() + 80 + "px");
        } else {
            this._$elContent.css("min-height", 0);
        }
    };
    SideMenu.prototype._bindScrollWindow = function () {
        // ナビゲーションが存在しない場合は処理しない
        if (this._$elParent.length === 0) {
            return;
        }
        var timer = null;
        $(w)
            .on(
                "scroll",
                function () {
                    // スクロールが止まってから処理を実行する
                    clearTimeout(timer);
                    timer = setTimeout(
                        function () {
                            this._changeTop();
                        }.bind(this),
                        99
                    );
                }.bind(this)
            )
            .trigger("scroll");
    };
    SideMenu.prototype._changeTop = function () {
        // メインコンテンツの高さが、ナビゲーションの高さよりも低かった場合処理しない
        if (this._$elMainContent.innerHeight() <= this._$el.outerHeight()) {
            return;
        }
        // ページのスクロール値がナビゲーションのエリアに差し掛かったら追従処理を実行
        if ($(w).scrollTop() > this._$elParent.offset().top) {
            var topVal = 0;
            // top値を決める
            // ナビゲーションエリアの限界値。コンテンツ領域をはみ出さないための処理
            if ($(w).scrollTop() + this._$el.outerHeight() >= this._$elMainContent.offset().top + this._$elMainContent.height()) {
                topVal = this._$elContent.height() - this._$el.outerHeight();
            } else {
                topVal = $(w).scrollTop() - this._$elParent.offset().top /*+ $(".js-header").outerHeight() + 20 */;
            }
            this._$el.css({
                top: topVal + "px",
            });
        } else {
            this._$el.css({
                top: 0,
            });
        }
    };
    SideMenu.prototype._startChangeTopTimer = function () {
        this._changeTopTimer = setInterval(
            function () {
                this._changeTop();
            }.bind(this),
            10
        );
    };
    SideMenu.prototype._stopChangeTopTimer = function () {
        clearInterval(this._changeTopTimer);
    };
})(window, document);

$(function () {
    var _ns = window.fjt;

    // IE判定してIEであればclassを付与
    var ua = window.navigator.userAgent.toUpperCase();
    if (ua.indexOf("MSIE") === -1 && ua.indexOf("TRIDENT") === -1) {
        // IE以外
    } else {
        // IE
        $("body").addClass("is-ie");
    }

    // MV
    $(".js-mv").slick({
        arrows: false,
        dots: false,
        fade: true,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
    });
    $('.js-mv').on('beforeChange', function(){
        $('.js-copyright').fadeOut(100);
    });
    $('.js-mv').on('afterChange', function(event, slick, currentSlide){
        $($('.js-copyright')[currentSlide]).fadeIn(100);
    });

    // カルーセル
    $(".js-slide").slick({
        arrows: true,
        dots: true,
        fade: true,
        autoplay: false,
        infinite: false,
    });

    var modalOption = {
        type: "inline",
        opacity: 1,
        fixedContentPos: true,
        fixedBgPos: true,
        gallery: {
            enabled: true,
        },
        closeBtnInside: false,
    };

    // モーダル

    $(".js-popup-modal[data-modal=activity]").magnificPopup(modalOption);
    $(".js-popup-modal[data-modal=activity2]").magnificPopup(modalOption);
    $(".js-popup-modal[data-modal=map]").magnificPopup(modalOption);
    $(".js-popup-modal").on("mfpOpen", function (e /*, params */) {
        var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if (windowWidth <= 960) {
            _ns.LockScroll.lock();
        }
    });
    $(".js-popup-modal").on("mfpClose", function (e /*, params */) {
        var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if (windowWidth <= 960) {
            _ns.LockScroll.release();
        }
    });

    // ヘッダーの制御
    new _ns.Header();

    // サイドナビの制御
    new _ns.SideMenu();

    // スクロールに伴うアニメーションライブラリ
    var windowHeight = $(window).innerHeight();
    AOS.init({
        offset: windowHeight / 3,
        duration: 600,
        easing: "ease-out",
    });

    // 高さを揃える
    $(".js-mathHeight").matchHeight();

    // more
    $(".js-more__trigger").on("click", function (e) {
        e.preventDefault();
        var $el = $(e.currentTarget);
        var $elTarget = $el.parents(".js-more").find(".js-more__content");

        if ($elTarget.length > 0) {
            $el.hide();
            $elTarget.slideDown(600);
        }
    });

    // スムーススクロール
    $(".js-scrollAnchor").on("click", function (e) {
        e.preventDefault();
        var $el = $(e.currentTarget);
        var $elTarget = $($el.attr("href"));
        var scrollTop = $elTarget.offset().top;
        var headerHeight = $(".js-header").outerHeight();
        $("html, body").animate({ scrollTop: scrollTop - headerHeight }, 400);
    });

    // Pagetopへ
    $(".js-pagetop__trigger").on("click", function (e) {
        e.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, 400);
    });

    // タブ
    $(".js-tab__trigger").on("click", function (e) {
        e.preventDefault();
        var $el = $(e.currentTarget);
        var $elTarget = $($el.attr("href"));
        var stateCurrent = "is-current";

        if ($el.hasClass(stateCurrent)) {
            return;
        }

        var $elParent = $el.parents(".js-tab");

        $elParent.find(".js-tab__trigger").removeClass(stateCurrent);
        $elParent.find(".js-tab__content").removeClass(stateCurrent);

        $el.addClass(stateCurrent);
        $elTarget.addClass(stateCurrent);

        var windowHeight = $(window).innerHeight();
        AOS.init({
            offset: windowHeight / 3,
            duration: 600,
            easing: "ease-out",
        });
    });
});
