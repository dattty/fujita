'use strict';

var gulp              = require('gulp'),
    header            = require('gulp-header'),
    imagemin          = require('gulp-imagemin'),
    pngquant          = require('imagemin-pngquant'),
    mozjpeg           = require('imagemin-mozjpeg'),
    autoprefixer      = require('gulp-autoprefixer'),
    csscomb           = require('gulp-csscomb'),
    groupCssMq        = require('gulp-group-css-media-queries'),
    nunjucksRender    = require('gulp-nunjucks-render'),
    plumber           = require('gulp-plumber'),
    concat            = require('gulp-concat'),
    browserSync       = require('browser-sync'),
    ssi               = require('browsersync-ssi'),
    htmlhint          = require('gulp-htmlhint'),
    eslint            = require('gulp-eslint'),
    sass              = require('gulp-sass'),
    replace           = require('gulp-replace'),
    watch             = require('gulp-watch'),
    del               = require('del'),
    runSequence       = require('run-sequence');


// ディレクトリ定義
const DIR_SRC  = 'src';
const DIR_DEV  = 'dev';
const DIR_DIST = 'dist';


// 出力形式
const BUILD_TYPE_DEV  = 'develop';
const BUILD_TYPE_DIST = 'production';

var buildType = BUILD_TYPE_DEV;


/**
 * dest先のフォルダを返す
 * @return {string} - ディレクトリ名
 */
var getDestDir = function() {
    return buildType === BUILD_TYPE_DIST ? DIR_DIST : DIR_DEV;
};


// destフォルダ消去
gulp.task('deleteDest', function(callback) {
    var stream = del( getDestDir() + '/**/*' );
    return stream;
});


// html生成
gulp.task('createHtml', function(callback) {
    var stream = gulp.src([
        DIR_SRC + '/**/*.njk',
        '!' + DIR_SRC + '/_layout/**/*',
        '!' + DIR_SRC + '/_partials/**/*'
    ])
    .pipe(nunjucksRender({
        path : DIR_SRC + '/'
    }))
    .pipe(gulp.dest(getDestDir()));

    return stream;
});


// 画像コピー
gulp.task('copyImg', function(){
    // ファイル複製
    var stream = gulp.src([
        DIR_SRC + '/images/**/*.*' // 全てをコピーする
    ])
    .pipe(gulp.dest( getDestDir() + '/images/' ));

    return stream;
});


// 画像圧縮
gulp.task('minifyImg', function(){
    var stream = gulp.src( getDestDir() + '/**/*.{png,jpg,gif}' )
    .pipe(imagemin(
        [
            pngquant({
                quality: '60-80', // 画像が真っ白になる現象が発生したら、これを外す。
                speed: 1
            }),
            mozjpeg({
              quality:72
            }),
            imagemin.optipng(),
            imagemin.gifsicle()
        ]
    ))
    .pipe(gulp.dest( getDestDir() ));

    return stream;
});


// jsコピー
gulp.task('copyJs', function() {
    var stream = gulp.src([
        DIR_SRC + '/**/*.{js,js.gz}'
    ])
    .pipe(gulp.dest( getDestDir() ));

    return stream;
});


// CSS生成
gulp.task('createCss', function(){
    var stream = gulp.src( DIR_SRC + '/*.scss' )
    .pipe(sass({ outputStyle: 'expanded' }).on('error', sass.logError))
    .pipe(groupCssMq({ // mediaquery 整頓
        log: true
    }))
    .pipe(plumber())
    .pipe(csscomb()) // css 整形
    .pipe(autoprefixer({ // css ベンダープレフィックス付与
        browsers: [ // コーディングガイドラインに準ずるバージョンとする
            'Chrome 56',
            'IE 11',
            'Edge 14',
            'Firefox 51',
            'Safari 10',
            'Android >= 4.4'
        ]
    }))
    .pipe(gulp.dest( getDestDir() ));

    return stream;
});


// CSSコピー
gulp.task('copyCss', function(){
    var stream = gulp.src( DIR_SRC + '/*.css' )
    .pipe(gulp.dest( getDestDir() ));

    return stream;
});


// CSS結合
gulp.task('concatCss', function(){
    var stream = gulp.src([
        DIR_SRC + '/_css/normalize.css',
        getDestDir() + '/base.css',
    ])
    .pipe(concat('common.css'))
    .pipe(gulp.dest( getDestDir() ));

    return stream;
});


// CSSの先頭ににcharsetを書き込む
// 全ての環境で等しく動作するように、一度デフォルトで設定されるcharsetを削除した上で改めて挿入する。
gulp.task('addCharsetCSS', function () {
    var stream = gulp.src([
        getDestDir() + '/base.css',
        getDestDir() + '/layout.css',
        getDestDir() + '/module.css',
        getDestDir() + '/utility.css'
    ])
    .pipe(replace(/@charset "UTF-8";\n/,''))
    .pipe(header('@charset "UTF-8";\n\n'))
    .pipe(gulp.dest( getDestDir() ));

    return stream;
});


// watch
gulp.task('watch', function(callback) {
    gulp.watch(DIR_SRC + '/**/*.njk', ['html']);
    gulp.watch(DIR_SRC + '/**/*.scss', ['css']);
    gulp.watch(DIR_SRC + '/js/**/*.{js,js.gz}', ['js']);
    gulp.watch(DIR_SRC + '/images/**/*.{png,jpg,gif,svg}', ['img']);
    gulp.watch(getDestDir() + '/**/*.{html,css,js,png,jpg,gif,svg}', ['reload']);
});


// 仮装サーバー
gulp.task('server', function() {
    var stream = browserSync.init({
        server: {
            baseDir: DIR_DEV,
            middleware: [
                // SSI使えるようにする
                ssi({
                    baseDir: __dirname + '/' + DIR_DEV,
                    ext: '.html'
                }),
                // gzip使えるようにする
                function(req, res, next) {
                    let url = req.url;
                    // リクエストURLに.jz.gzが含まれる場合
                    if (url.indexOf('.js.gz') > -1) {
                        res.setHeader('Content-Encoding', 'gzip');
                        res.setHeader('Content-Type', 'text/javascript');
                    // リクエストURLに.css.gzが含まれる場合
                    } else if (url.indexOf('.css.gz') > -1) {
                        res.setHeader('Content-Encoding', 'gzip');
                        res.setHeader('Content-Type', 'text/css');
                    // リクエストURLに.html.gzが含まれる場合
                    } else if (url.indexOf('.html.gz') > -1) {
                        res.setHeader('Content-Encoding', 'gzip');
                        res.setHeader('Content-Type', 'text/html');
                    }
                    next();
                }
            ]
        },
        reloadDelay: 200,
        reloadDebounce: 200 // ディレイ中はreload禁止
    });

    return stream;
});


// ブラウザ更新
gulp.task('reload', function() {
    return browserSync.reload();
});


// 初期化
gulp.task('init', function(callback) {
    return runSequence(
        'deleteDest',
        callback
    );
});


// HTML周りの処理
gulp.task('html', function(callback) {
    return runSequence(
        'createHtml',
        callback
    );
});


// 画像周りの処理
gulp.task('img', function(callback) {
    return runSequence(
        'copyImg',
        'minifyImg',
        callback
    );
});


// JS周りの処理
gulp.task('js', function(callback) {
    if(buildType === BUILD_TYPE_DIST) { // 納品データ出力時
        return runSequence(
            'copyJs',
            callback
        );
    } else { // 開発データ出力時
        return runSequence(
            'copyJs',
            callback
        );
    }
});


// CSS周りの処理
gulp.task('css', function(callback) {
    return runSequence(
        'createCss',
        ['copyCss', 'concatCss'],
        'addCharsetCSS',
        callback
    );
});


// HTMLバリデーション
gulp.task('validateHtml', function(){
    return gulp.src( getDestDir() + '/**/*.html' )
    .pipe(htmlhint('.htmlhintrc')) // ルールを読み込む
    .pipe(htmlhint.failReporter()) // エラーで強制的に終了させる
});


// Javascriptバリデーション
gulp.task('validateJs', function() {
    return gulp.src([
        dir.src + '/**/*.{js,js.gz}',
        '!' + dir.src + '/**/libs/*.{js,js.gz}'
    ])
    .pipe(eslint({ useEslintrc: true }))
    .pipe(eslint.format())
    .pipe(eslint.failOnError())
});


// gulp実行 開発データ出力
gulp.task('default', function(callback) {
    return runSequence(
        'init',
        [
            'html',
            'img',
            'js',
            'css'
        ],
        'server',
        'watch',
        callback
    );
});


// gulp実行 納品データ出力
// gulp dist
gulp.task('dist', function(callback) {
    buildType = BUILD_TYPE_DIST;
    return runSequence(
        'init',
        [
            'html',
            'img',
            'js',
            'css'
        ],
        callback
    );
});
